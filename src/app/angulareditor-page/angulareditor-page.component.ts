import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-angulareditor-page',
  templateUrl: './angulareditor-page.component.html',
  styleUrls: ['./angulareditor-page.component.sass'],
})
export class AngulareditorPageComponent implements OnInit {
  @Output() htmlContent = new EventEmitter<string>();
  public htmlWritted: string = '';

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    minHeight: '5rem',
    placeholder: 'Escribe aquí el artículo...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [['bold']],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText',
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
  };

  constructor() {}

  ngOnInit(): void {
    //
  }

  ngDoCheck() {
    this.htmlContent.emit(this.htmlWritted);
  }
}
